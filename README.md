# fantom-web

Web-application for computes and verifies a Boneh-Lynn-Shacham signature in a simulated conversation between HTTP-server and client.

run ./web in the console

start http://localhost:8080 in your browser

you can get and verify public key, make signature with your message and verify it with server-message