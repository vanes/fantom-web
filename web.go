package main

import (
    "encoding/json"
    "html/template"
    "net/http"
    "path"
    "pbc"
    "fmt"
    "strings"
    "crypto/sha256"
)

var params = pbc.GenerateA(160, 512)
var pairing = params.NewPairing()
var g = pairing.NewG2().Rand()	
var privKey = pairing.NewZr().Rand()

type messageData struct {
    message   string
    signature []byte
}

type Data struct {
    Quote string
}

// Default Request Handler
func defaultHandler(w http.ResponseWriter, r *http.Request) {

    fp := path.Join("html/", "index.html")
    tmpl, err := template.ParseFiles(fp)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    if err := tmpl.Execute(w, nil); err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
    }

}

// AJAX Request Handler
func ajaxHandler(w http.ResponseWriter, r *http.Request) {
    //parse request to struct
    var d Data
    err := json.NewDecoder(r.Body).Decode(&d)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
    }

    // create json response from struct
    a, err := json.Marshal(d)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
    }
    w.Write(a)
}

func getpbHandler(w http.ResponseWriter, r *http.Request) {
	pubKey := pairing.NewG2().PowZn(g, privKey)
	fmt.Fprint(w, pubKey.String())
}

func verifyHandler(w http.ResponseWriter, r *http.Request) {
	pubkey := r.FormValue("publickey")
	pubKey := pairing.NewG2().PowZn(g, privKey)
	
	if strings.Compare(pubKey.String(), pubkey) == 0 {
      //fmt.Fprint(w, "Verified") 
      w.Write([]byte("Verified"))
    } else {
	  //fmt.Fprint(w, "Nor verified")
	  w.Write([]byte("Not Verified"))
	}	
}

func getsignatureHandler(w http.ResponseWriter, r *http.Request) {
	message := "Hello, Fantom Foundation!" 
    w.Write([]byte(message))

}

func signHandler(w http.ResponseWriter, r *http.Request) {
	pubkey := r.FormValue("publickey")
	messageB := r.FormValue("message")
	messageA := "Hello, Fantom Foundation!"


    hA := pairing.NewG1().SetFromStringHash(messageA, sha256.New())
    signatureA := pairing.NewG2().PowZn(hA, privKey)
    
    pubKey := pairing.NewG2().PowZn(g, privKey)
	hB := pairing.NewG1().SetFromStringHash(messageB, sha256.New())
	temp1 := pairing.NewGT().Pair(hB, pubKey)
	temp2 := pairing.NewGT().Pair(signatureA, g)
	if strings.Compare(pubKey.String(), pubkey) == 0 {
      if !temp1.Equals(temp2) {
          w.Write([]byte("Error! Check your message"))
      } else {
		  w.Write([]byte("Signature verified correctly! \n e(h,g^x)=e(sig,g)\n" + temp1.String() + "\n" + temp2.String()))
          //fmt.Fprint(w, "Signature verified correctly - e(h,g^x)=e(sig,g)")
      }
    } else {
	  w.Write([]byte("Error! Public key wrong"))
	  //fmt.Fprint(w, "Public key wrong!")
	  //fmt.Fprint(w, pairing.NewG2().SetCompressedBytes([]byte(pubkey)))
	} 
}



func main() {
	http.HandleFunc("/", defaultHandler)
    http.HandleFunc("/ajax", ajaxHandler)
    http.HandleFunc("/getpublickey", getpbHandler)
    http.HandleFunc("/verify", verifyHandler)
    http.HandleFunc("/getmessage", getsignatureHandler)
    http.HandleFunc("/sign", signHandler)
    
    fs := http.FileServer(http.Dir("./html/static"))
    http.Handle("/static/", http.StripPrefix("/static/", fs))
    
    http.ListenAndServe(":8080", nil)
}
